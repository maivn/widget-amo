define(['jquery', 'lib/components/base/modal', './js/utils.js'], function ($, Modal, Utils) {
    return function () {
        const self = this;
        const code = "gen_doc";
        const eventNS = '.' + code;
        const clickNS = `click${eventNS}`;

        this.widgetCode = 'gendoc';
        this.utils = new Utils(self);


        let $rightWidget;

        const supportedAreas = {
            advancedSettings: 'widget.advanced_settings',
            settings: 'settings.widgets',
            lcard: 'leads.card'
        };

        let currentArea;

        Object.defineProperties(self, {
            'cssPath': {
                get() {
                    const cssPath = self.params.path + '/css/';

                    Object.defineProperty(this, 'cssPath', {
                        value: cssPath
                    });

                    return cssPath;
                },
                configurable: true
            },
            'baseTemplates': {
                get() {
                    const templatePath = self.params.path.substr(1) + '/templates/';

                    Object.defineProperty(this, 'baseTemplates', {
                        value: templatePath
                    });

                    return templatePath;
                },
                configurable: true
            }
        });

        window.th = self;
        window.AMOCRM = AMOCRM;

        this.downloadFile = function downloadFile(documentUrl) {
            $('<iframe/>').attr({
                src: decodeURIComponent(documentUrl).replace(/#/g, '%23').replace(/\s/g, '%20'),
                style: 'visibility:hidden;display:none'
            }).appendTo('body');
        };

        this.downloadDocument = async function downloadDocument() {
            const requestParams = $.param(self.utils.defaultParams);
            self.downloadFile(this.dataset.url + '?' + (requestParams ? requestParams : ''));
        };


        this.deleteDocument = async function deleteDocument() {
            self.loader($rightWidget.find('.widget-wrap'), true);
            const $button = $(this);
            const id = this.dataset.id;
            try {
                const document = await self.utils.post(self.utils.getUrl('documents', 'remove'), {
                    'ID': id,
                });

                $button.parents('.row').remove();
            } catch (error) {
                let err = error && error.responseJSON && error.responseJSON.errors && error.responseJSON.errors[0] && error.responseJSON.errors[0].detail;
                self.utils.error(err, self.i18n('error.header'));
            }
            self.loader($rightWidget.find('.widget-wrap'), false);
        };

        this.generateTemplate = async function generateTemplate() {
            const $this = $(this);
            const templateId = $this.data("id");
            const typeDocument = $this.data("type");
            const contacts = self.getContactsByCurrentLead();

            let contactId;
            try {
                contactId = contacts.length > 1 ? await self.openModalChooseContact(contacts) : null;
            } catch (e) {
                return false;
            }
            contactId = contactId || !contacts.length ? contactId : contacts[0].id;
            const generateParams = [String(self.getCurrentLeadId()), contactId, templateId, typeDocument];
            self.generateDocumentByLead(...generateParams);
            return false;
        };

        this.generateDocumentByLead = async function generateDocumentByLead(leadId, contactId, templateId, typeDocument) {
            self.loader($rightWidget.find('.widget-wrap'), true);
            const user = AMOCRM.constant('user');
            try {
                const document = await self.utils.post(self.utils.getUrl('documents', 'generate'), {
                    'lead_id': leadId,
                    'contact_id': contactId,
                    'template_id': templateId,
                    'type_document': typeDocument,
                    'user_id': user.id
                });

                const requestParams = $.param(self.utils.defaultParams);
                self.downloadFile(document.url + '?' + (requestParams ? requestParams : ''));
                self.appendDocument(document);
            } catch (error) {
                let err = error && error.responseJSON && error.responseJSON.errors && error.responseJSON.errors[0] && error.responseJSON.errors[0].detail;
                self.utils.error(err, self.i18n('error.header'));
            }

            self.loader($rightWidget.find('.widget-wrap'), false);
        };

        this.appendDocument = async function appendDocument(document) {
            if (!this.documentTemplate) {
                this.documentTemplate = await self.utils.getTemplatePromise('document');
            }

            if (!this.$documents) {
                this.$documents = $('.documents-wrapper', self.utils.id());
            }

            window.requestAnimationFrame(() => {
                this.$documents.append(this.documentTemplate.render({
                    document
                }));
            });
        };


        this.openModalChooseContact = async function (contacts) {
            return new Promise(function (resolve, reject) {
                const modalData = {
                    contacts
                };
                const modalOptions = {
                    id: self.utils.id('choose-contact', false),
                    destroy() {
                        reject();
                    }
                };
                self.utils.createModal('choose-contact', modalData, modalOptions).then(({$modalBody, modal}) => {
                    $modalBody.on('click', '.contact-choose-button', function () {
                        resolve(this.dataset.id);
                        modal.destroy();
                    }).trigger('modal:centrify').trigger('modal:loaded');
                }).catch(() => {
                    reject();
                });
            });
        };

        this.loader = async function loader($block, show = true) {
            if (!this.loaderTemplate) {
                this.loaderTemplate = await self.utils.getTemplatePromise('loader');
            }

            if (show) {
                if (!$block.find('.overlay-loader').length) {
                    $block.append(this.loaderTemplate.render());
                }
            } else {
                $block.find('.overlay-loader').remove();
            }
        };

        this.getSettingsForRightWidget = function getSettingsForRightWidget() {
            return self.utils.get(self.utils.getUrl('widget', 'settings'), {
                'lead_id': self.getCurrentLeadId()
            });
        };

        this.getCurrentLeadId = function getCurrentLeadId() {
            if (currentArea === supportedAreas.lcard) {
                return parseInt(AMOCRM.data.current_card.id);
            }
            return null;
        };

        this.getContactsByCurrentLead = function getContactsByCurrentLead() {
            let contacts = [];
            if (currentArea === supportedAreas.lcard) {
                AMOCRM.data.current_card.linked_forms.form_models.models.forEach(function (item) {
                    if (item.attributes.ELEMENT_TYPE === "1" && item.attributes.ID && item.attributes['contact[NAME]']) {
                        contacts.push({
                            id: parseInt(item.attributes.ID),
                            name: item.attributes['contact[NAME]']
                        });
                    }
                });
            }

            return contacts;
        };

        this.renderWidget = function () {
            let rendered = false;
            return async function renderWidget() {
                if (rendered) {
                    return;
                }
                rendered = true;

                try {
                    const [template, {templates, documents}] = await Promise.all([self.utils.getTemplatePromise('widget'), self.getSettingsForRightWidget()]);

                    $rightWidget.find(' .widget-wrap').html(template.render({
                        templates,
                        documents,
                    })).css('min-height', 'auto');
                } catch (error) {
                    self.utils.error(error, "ошибка");
                }
            };
        }();

        this.callbacks = {
            render() {
                currentArea = AMOCRM.getV3WidgetsArea();
                if (currentArea === supportedAreas.settings || currentArea === supportedAreas.advancedSettings || AMOCRM.data.current_card.id === 0) {
                    return true;
                }
                self.utils.appendCss('widget');
                self.render_template({
                    caption: {
                        class_name: 'title-wrap',
                        html: ''
                    },
                    body: '',
                    render: '<div class="wrapper"><div class="widget-wrap"></div></div>'
                });
                $rightWidget = $('.card-widgets__widget[data-code="' + self.params.widget_code + '"]', '#nano-card-widgets').attr('id', self.utils.id('', false));
                $rightWidget.attr("style", "background: rgb(46, 54, 64);");
                self.loader($rightWidget.find('.widget-wrap'));
                return true;
            },
            destroy() {
                $(document).off(eventNS);

                return true;
            },
            init() {
                return true;
            },
            bind_actions() {
                if (self.params.widget_active === 'N') {
                    return true;
                }
                switch (currentArea) {
                    case supportedAreas.advancedSettings: {
                        break;
                    }
                    case supportedAreas.settings: {
                        break;
                    }
                    case supportedAreas.lcard: {
                        if (AMOCRM.data.current_card.id === 0) {
                            return true;
                        }
                        $("#gendoc_widget").off(clickNS, '.document-download').on(clickNS, '.document-download', self.downloadDocument)
                            .off(clickNS, '.document-delete').on(clickNS, '.document-delete', self.deleteDocument)
                            .off(clickNS, '.generate-template').on(clickNS, '.generate-template', self.generateTemplate)
                        $rightWidget.one(clickNS, self.renderWidget);
                        break;
                    }
                }
                return true;
            },
            settings() {
                if (AMOCRM.lang_id == "ru") {
                    $("#" + code + "_custom_content").html("<div><iframe width='560' height='315' src='https://www.youtube.com/embed/fKXaJGQqJ5I' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe></div><label><input type='checkbox' name='agreement' value='1'/>" + self.i18n('settings.agreement') + "</label>").parent().show()
                }
                return true;
            },
            async advancedSettings() {
                self.utils.appendCss('advanced');
                const mainTemplatePromise = self.utils.getTemplatePromise('advanced_settings');
                const $mainTemplate = $((await mainTemplatePromise).render(self.utils.defaultParams));
                $('#work_area').addClass('gendoc').find('.list__body-right__top').hide();
                $('#work-area-' + self.params.widget_code).append($mainTemplate);
            },
            onSave: function () {
                return true;
            },
            destroy: function () {

            }
        };
        return this;
    };
});