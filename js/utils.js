define(['jquery', 'lib/components/base/modal'], function ($, Modal) {
    return function (widget) {
        const self = this;

        this.request = function (requestType, url, params = {}) {
            return new Promise(function (resolve, reject) {
                $[requestType](url, params).done(response => {
                    if (!('success' in response)) {
                        return resolve(response);
                    }

                    if (!response.success) {
                        return reject(response);
                    }

                    return resolve(response.data);
                }).fail(response => {
                    return reject(response);
                });
            });
        };

        this.defaultParams = function () {
            return {
                amo_domain: widget.system().subdomain,
                amo_email: widget.system().amouser,
                amo_hash: widget.system().amohash
            };
        }();


        this.get = function (url, params = {}) {
            return self.request('get', url, params);
        };

        this.getJson = function (url, params = {}) {
            return self.request('getJSON', url, params);
        };

        this.post = function (url, params = {}) {
            return self.request('post', url, params);
        };

        this.getUrl = function (controller, method, params) {
            const requestParams = $.param($.extend(params, self.defaultParams));
            return '/' + '/api.gen-doc.tk/' + controller + '/' + method + '?' + (requestParams ? requestParams : '');
        };

        this.getTemplatePromise = function (templateName) {
            return new Promise(function (resolve) {
                widget.render({
                    href: widget.baseTemplates + templateName + '.twig',
                    base_path: '/',
                    v: widget.version,
                    load(template) {
                        resolve({
                            render: (data = {}) => template.render(Object.assign({}, data, {
                                i18n: widget.i18n.bind(widget),
                                base_templates: widget.baseTemplates
                            }))
                        });
                    }
                });
            });
        };


        this.appendCss = function (fileName, isFullPath = false) {
            const fullPath = isFullPath ? fileName : widget.cssPath + fileName + '.css';

            if ($(`link[href="${fullPath}"]`).length) {
                return false;
            }

            $('head').append(`<link rel="stylesheet" href="${fullPath}" type="text/css"/>`);

            return true;
        };

        this.id = function (postfix = '', hash = true) {
            return (hash ? '#' : '') + widget.widgetCode + '_widget' + (postfix ? '_' + postfix : '');
        };

        this.error = function (text, header = '') {
            if (!text || typeof text !== 'string') {
                return;
            }

            AMOCRM.notifications.add_error({
                header: 'Генератор документов: ' + header,
                text: text,
                date: Math.ceil(Date.now() / 1000)
            });
        };

        this.createModal = function (templateName = null, templateData = {}, options = {}) {
            return new Promise(function (resolve) {
                (templateName ? self.getTemplatePromise(templateName) : Promise.resolve()).then(template => {
                    let $modalBodyTemp = null;

                    const modal = new Modal({
                        default_overlay: options.default_overlay || false,
                        destroy() {
                            if (options.destroy) {
                                options.destroy();
                            }
                        },
                        init($modalBody) {
                            if (options.attr) {
                                $modalBody.attr(options.attr);
                            }

                            if (options.add_class) {
                                $modalBody.addClass(options.add_class);
                            }

                            if (options.id) {
                                $modalBody.attr('id', options.id);
                            }

                            if (options.css) {
                                $modalBody.css(options.css);
                            }

                            if (options.html) {
                                $modalBody.html(options.html);
                            }

                            if (template) {
                                templateData.base_templates = widget.baseTemplates;
                                $modalBody.html(template.render(templateData));
                            }

                            $modalBodyTemp = $modalBody;
                        }
                    });

                    resolve({$modalBody: $modalBodyTemp, modal});
                });
            });
        };
    }
});